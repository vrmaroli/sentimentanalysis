"""
Sentiment Analysis using nltk.classify.NaiveBayesClassifier
Vishnu Ramesh Maroli

"""

from nltk.classify import NaiveBayesClassifier
from nltk.corpus import movie_reviews
from nltk.util import ngrams
import random

def get_features(num,text):
    """
        Feature set generation for training data
    """
    filtered_text = [x for x in text if len(x)>=3]
    grams = list(ngrams(filtered_text, num))
    return dict([(" ".join(gram), True) for gram in grams])

def get_features_from_string(num,text):
    """
        Feature set generation for test string
    """
    filtered_text = " ".join((char if char.isalpha() else " ") for char in text).split()
    filtered_filtered_text = [x for x in filtered_text if len(x)>=3]
    grams = ngrams(filtered_filtered_text, num)
    testData = dict([(" ".join(gram), True) for gram in grams])
    return testData

def return_classifier(num):
    trainingData = [(get_features(num,movie_reviews.words(fileid)), 'Negative') for fileid in movie_reviews.fileids('neg')] + [(get_features(num,movie_reviews.words(fileid)), 'Positive') for fileid in movie_reviews.fileids('pos')]
    random.shuffle(trainingData)
    classifier = NaiveBayesClassifier.train(trainingData)
    return classifier
