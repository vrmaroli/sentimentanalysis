import csv
import sugandh1
import sugandh2

if __name__ == '__main__':
    f = open("amazonReviewSnippets_GroundTruth.txt")

    with open('amazon_SVM_Comparison.csv', 'w') as csvfile:
        fieldnames = ['Actual', 'SVCS', 'NaivebayesS','Sentence']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        # c1 = sentimentAnalysis.return_classifier(1)
        # c2 = sentimentAnalysis.return_classifier(2)
        # c3 = sentimentAnalysis.return_classifier(3)
        for line in f:
            part1,part2,part3 = line.split("\t")
            #part1 = int(part1)
            part2 = float(part2)/4
            if(part2>=0.0):
                text1 = "Positive"
            else:
                text1 = "Negative"
            sugResult1 = sugandh1.sentiment(part3)
            sugResult2 = sugandh2.sentiment(part3)
            # try:
            # result = vaderSentiment.sentiment(part3)
            # ourResult1 = c1.classify(sentimentAnalysis.get_features_from_string(1,part3))
            # ourResult2 = c2.classify(sentimentAnalysis.get_features_from_string(2,part3))
            # ourResult3 = c3.classify(sentimentAnalysis.get_features_from_string(3,part3))
            # sugResult = sugandh.sentiment(part3)
            # if(sentence.sentiment.polarity>=0.0):
            #     text2 = "Positive"
            # else:
            #     text2 = "Negative"
            # if(result['compound']>=0.0):
            #     text3 = "Positive"
            # else:
            #     text3 = "Negative"
            writer.writerow({'Actual': text1, 'SVCS': sugResult2, 'NaivebayesS': sugResult1, 'Sentence': part3})
            # except:
                # print("Error with : " + str(part3))
    f.close()